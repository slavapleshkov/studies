<?php
//array_keys — Возвращает все или некоторое подмножество ключей массива
print_r(array_keys([0 => 100, "color" => "red"]));
print_r(array_keys(["blue", "red", "green", "blue", "blue"], "blue"));
//array_merge_recursive — Рекурсивное слияние одного или более массивов
var_dump(array_merge_recursive(["color" => ["favorite" => "red"], 5], [10, "color" => ["favorite" => "green", "blue"]]));
//array_merge — Сливает один или большее количество массивов
var_dump(array_merge(["color" => ["favorite" => "red"], 5], [10, "color" => ["favorite" => "green", "blue"]]));
//array_pad — Дополнить массив определенным значением до указанной длины
var_dump(array_pad([12, 10, 9], 5, 0));
//array_pop — Извлекает последний элемент массива
$array_pop = ["orange", "banana", "apple", "raspberry"];
var_dump(array_pop($array_pop));
//array_product — Вычислить произведение значений массива
echo array_product([5, 5, 5]);
//array_push — Добавляет один или несколько элементов в конец массива
$array_push = ["orange", "banana"];
var_dump(array_push($array_push, "apple", "raspberry"));
//array_rand — Выбирает один или несколько случайных ключей из массива
var_dump(array_rand(["Neo", "Morpheus", "Trinity", "Cypher", "Tank"], 2));
//array_replace_recursive — Рекурсивно заменяет элементы первого массива элементами переданных массивов
$array_replace_recursive1 = ['citrus' => ["orange"], 'berries' => ["blackberry", "raspberry"]];
$array_replace_recursive2 = ['citrus' => ['pineapple'], 'berries' => ['blueberry']];
var_dump(array_replace_recursive($array_replace_recursive1, $array_replace_recursive2));
//array_replace — Заменяет элементы массива элементами других переданных массивов
$array_replace1 = ["orange", "banana", "apple", "raspberry"];
$array_replace2 = [0 => "pineapple", 4 => "cherry"];
$array_replace3 = [0 => "grape"];
var_dump(array_replace($array_replace1, $array_replace2, $array_replace3));
//array_reverse — Возвращает массив с элементами в обратном порядке
$array_replace = ["php", "oop", "mysql", "sql"];
var_dump(array_reverse(["php", "oop", "mysql", "sql"], true));
//range — Создает массив, содержащий диапазон элементов
var_dump(range(0, 12));
//shuffle — Перемешивает массив
$range = range(1, 20);
shuffle($range);
var_dump($range);

