<?php

class MyClass
{
    public $public = 'Public';
    private $private = 'Private';
    protected $protected = 'Protected';

    function printHello()
    {
        echo $this->public;
        echo $this->private;
        echo $this->protected;
    }
}

$obj = new MyClass();
$obj->printHello();