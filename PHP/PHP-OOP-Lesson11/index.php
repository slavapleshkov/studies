<?php

trait Hello
{
    public function sayHello()
    {
        echo 'Hello ';
    }
}

trait World
{
    public function sayWorld()
    {
        echo 'World';
    }
}

class MyClass
{
    use Hello, World;
}

$o = new MyClass();
$o->sayHello();
$o->sayWorld();
