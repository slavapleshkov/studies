<?php
//money_format — Форматирует число как денежную величину
echo money_format('%i', 1234.56);
//nl2br — Вставляет HTML-код разрыва строки перед каждым переводом строки
echo nl2br("foo - это вам не\n bar");
//number_format — Форматирует число с разделением групп
echo number_format(1234.56);
//parse_str — Разбирает строку в переменные
parse_str("My Value=Something");
echo $My_Value;
//print — Выводит строку
print('Hello');
//similar_text — Вычисляет степень похожести двух строк
$similar_text = similar_text('bafoobar', 'bafooba', $perc);
echo "сходство: $similar_text ($perc %)\n";
//str_pad — Дополняет строку другой строкой до заданной длины
echo str_pad("Alien", 10);
//str_repeat — Возвращает повторяющуюся строку
echo str_repeat("-=", 10);
//str_replace — Заменяет все вхождения строки поиска на строку замены
echo str_replace('ll', 'LL', 'good golly miss molly!');
//str_shuffle — Переставляет символы в строке случайным образом
echo str_shuffle('abcdef');
//str_split — Преобразует строку в массив
var_dump(str_split('Hello Friend', '1'));