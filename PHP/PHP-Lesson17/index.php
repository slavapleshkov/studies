<?php
//array_unshift — Добавляет один или несколько элементов в начало массива
$array1 = array("orange", "banana");
array_unshift($array1,"apple");
print_r($array1);
//array_push — Добавляет один или несколько элементов в конец массива
$array2 = array("orange", "banana");
array_push($array2, "apple");
print_r($array2);
//array_shift — Извлекает первый элемент массива
$array3 = array("orange", "banana");
array_shift($array3);
print_r($array3);
//array_pop — Извлекает последний элемент массива
$array4 = array("orange", "banana");
array_pop($array4);
print_r($array4);
//array_sum — Вычисляет сумму значений массива
$array5 = array(1,2,3,5,7,8);
echo array_sum($array5).'</br>';
//array_search — Осуществляет поиск данного значения в массиве и возвращает ключ первого найденного элемента в случае удачи
$array6 = array("orange", "banana");
$key = array_search("banana",$array6);
echo $key.'</br>';