<?php
//sin — Синус
echo sin(60);
//cos — Косинус
echo cos(M_PI);
//tan — Тангенс
echo tan(M_PI_4);
//asin — Арксинус
echo asin(60);
//acos — Арккосинус
echo acos(M_PI);
//atan — Арктангенс
echo atan(M_PI_4);
//sinh — Гиперболический синус
echo sinh(60);
//cosh — Гиперболический косинус
echo cosh(M_PI);
//tanh — Гиперболический тангенс
echo tanh(M_PI_4);
//asinh — Гиперболический арксинус
echo asinh(60);
//acosh — Гиперболический арккосинус
echo acosh(M_PI);
//atanh — Гиперболический арктангенс
echo atanh(M_PI_4);
//atan2 — Арктангенс двух переменных
echo atan2(3,2);
