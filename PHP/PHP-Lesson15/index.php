<?php
//array — Создает массив
$a = array(1, 2, 3, 4);
$b = array(
    "age" => 22,
    "name" => "Alex",
    "schoolBoy" => true
);
$c = [1, 2, 3, 4];
$d = [
    "age" => 22,
    "name" => "Alex",
    "schoolBoy" => true
];
