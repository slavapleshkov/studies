<?php
//addcslashes — Экранирует cтроку слешами в стиле языка C
$addcslashes = addcslashes("zoo['.']", 'z..A');
var_dump($addcslashes);
//addslashes — Экранирует строку с помощью слешей
$addslashes =  addslashes("O'Reilly?");
var_dump($addslashes);
//bin2hex — Преобразует бинарные данные в шестнадцатеричное представление
$bin2hex =  bin2hex('Hello');
var_dump($bin2hex);
//hex2bin — Преобразует шестнадцатеричные данные в двоичные
$hex2bin = hex2bin("48656c6c6f");
var_dump($hex2bin);
//chr — Возвращает символ по коду ASCII
$str = chr(240) . chr(159) . chr(144) . chr(152);
echo $str;
//chunk_split — Разбивает строку на фрагменты
$chunk_split = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ";
echo chunk_split($chunk_split, 4,"\n");
//convert_uuencode — Кодирует строку в формат uuencode
$convert_uuencode = convert_uuencode('Hello');
var_dump($convert_uuencode);
//convert_uudecode — Декодирует строку из формата uuencode в обычный вид
$convert_uudecode = convert_uudecode("+22!L;W9E(%!(4\"$`\n`");
var_dump($convert_uudecode);


