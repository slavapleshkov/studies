<?php
// $ объявления переменной
$number=5;
//gettype — Возвращает тип переменной
echo gettype('Hello, world PHP');
//echo — Выводит одну или более строк
echo ('Hello, world PHP');
//var_dump — Выводит информацию о переменной
var_dump('Hello, world PHP');
//print_r — Выводит удобочитаемую информацию о переменной
print_r('Hello, world PHP');
//print — Выводит строку
print('Hello, world PHP');