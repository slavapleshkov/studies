<?php

class MyClass
{
    public $public = 'Public';
    private $private = 'Private';
    protected $protected = 'Protected';

    function printHello()
    {
        echo $this->public;
        echo $this->private;
        echo $this->protected;
    }
}

class TwoClass extends MyClass{

}
$obj = new TwoClass();
$obj->printHello();