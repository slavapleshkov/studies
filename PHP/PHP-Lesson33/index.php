<?php
//array_search — Осуществляет поиск данного значения в массиве и возвращает ключ первого найденного элемента в случае удачи
var_dump(array_search('green', ['blue', 'red', 'green', 'red']));
//array_shift — Извлекает первый элемент массива
$array_shift = ["orange", "banana", "apple", "raspberry"];
var_dump(array_shift($array_shift));
//array_slice — Выбирает срез массива
var_dump(array_slice(["a", "b", "c", "d", "e"], 2));
//array_splice — Удаляет часть массива и заменяет её чем-нибудь ещё
$input = array("red", "green", "blue", "yellow");
var_dump(array_splice($input, 2));
//array_sum — Вычисляет сумму значений массива
var_dump(array_sum([2, 4, 6, 8]));
//array_unique — Убирает повторяющиеся значения из массива
var_dump(array_unique([2, 4, 6, 8, 2]));
//array_unshift — Добавляет один или несколько элементов в начало массива
$array_unshift = ["orange", "banana"];
array_unshift($array_unshift, "apple", "raspberry");
var_dump($array_unshift);
//array_values — Выбирает все значения массива
var_dump(array_values(["size" => "XL", "color" => "gold"]));
//compact — Создает массив, содержащий названия переменных и их значения
$compact1 = "San Francisco";
$compact2 = "Ukraine";
var_dump(compact('compact1', 'compact2'));
//count — Подсчитывает количество элементов массива или что-то в объекте
var_dump(count(['red', 'apple', 'orange']));
//current — Возвращает текущий элемент массива
$current = ['foot', 'bike', 'car', 'plane'];
var_dump(current($current));
//end — Устанавливает внутренний указатель массива на его последний элемент
$end = ['foot', 'bike', 'car', 'plane'];
var_dump(end($end));
//next — Перемещает указатель массива вперед на один элемент
$next = ['foot', 'bike', 'car', 'plane'];
var_dump(next($next));
//reset — Устанавливает внутренний указатель массива на его первый элемент
$reset = ['foot', 'bike', 'car', 'plane'];
current($reset);
next($reset);
reset($reset);
echo current($reset);
//prev — Передвигает внутренний указатель массива на одну позицию назад
$prev = ['foot', 'bike', 'car', 'plane'];
var_dump(prev($prev));
//extract — Импортирует переменные из массива в текущую таблицу символов
$size = "large";
$var_array = ["color" => "blue", "size" => "medium", "shape" => "sphere"];
extract($var_array, EXTR_PREFIX_SAME, "prefix");
echo "$color, $size, $shape, $prefix_size\n";
//in_array — Проверяет, присутствует ли в массиве значение
var_dump(in_array('Mac', ["Mac", "NT", "Irix", "Linux"]));
//key — Выбирает ключ из массива
$array = ['fruit1' => 'apple', 'fruit2' => 'orange', 'fruit3' => 'grape', 'fruit4' => 'apple', 'fruit5' => 'apple'];
while ($fruit_name = current($array)) {
    if ($fruit_name == 'apple') {
        echo key($array) . '<br />';
    }
    next($array);
}
