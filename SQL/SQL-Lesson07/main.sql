/*
AUTO INCREMENT позволяет создавать уникальный номер автоматически, когда новая запись вставляется в таблицу.
*/
CREATE TABLE table_name (
    ID int NOT NULL AUTO_INCREMENT,
    LastName varchar(255),
    FirstName varchar(255),
    Age int,
    PRIMARY KEY (ID)
);