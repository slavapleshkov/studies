-- Оператор UPDATE используется для изменения существующих записей в таблице.
UPDATE table_name
SET ContactName='Juan';
UPDATE table_name
SET ContactName = 'Alfred Schmidt' WHERE ID = 1;