-- Оператор DROP DATABASE используется для удаления существующей базы данных SQL.
DROP DATABASE databasename;
-- Оператор DROP TABLE используется для удаления существующей таблицы в базе данных.
DROP TABLE table_name;