/*
Оператор BETWEEN выбирает значения в заданном диапазоне. Значения могут быть числами, текстом или датами.
Оператор BETWEEN включен: включены начальные и конечные значения.
*/
SELECT column_name(s)
FROM table_name
WHERE column_name BETWEEN value1 AND value2;