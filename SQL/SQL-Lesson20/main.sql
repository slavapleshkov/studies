-- Инструкция INSERT INTO используется для вставки новых записей в таблицу.
INSERT INTO table_name (column1, column2, column3)
VALUES (value1, value2, value3);
INSERT INTO table_name
VALUES (value1, value2, value3);