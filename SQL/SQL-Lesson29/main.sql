-- Функция COUNT () возвращает количество строк, соответствующих заданным критериям.
SELECT COUNT(column_name)
FROM table_name
WHERE condition;
-- Функция AVG () возвращает среднее значение числового столбца.
SELECT AVG(column_name)
FROM table_name
WHERE condition;
-- Функция SUM () возвращает общую сумму числового столбца.
SELECT SUM(column_name)
FROM table_name
WHERE condition;