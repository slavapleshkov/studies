/*
граничение DEFAULT используется для предоставления значения по умолчанию для столбца.
Значение по умолчанию будет добавлено ко всем новым записям, если другое значение не указано.
*/
CREATE TABLE table_name (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    City varchar(255) DEFAULT 'Sandnes'
);
-- DEFAULT on ALTER TABLE
ALTER TABLE table_name ALTER City
SET DEFAULT 'Sandnes';
-- DROP a DEFAULT Constraint
ALTER TABLE table_name ALTER City
DROP DEFAULT;