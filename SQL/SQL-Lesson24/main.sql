/*
Предложение WHERE может быть объединено с операторами AND, OR.
Операторы AND и OR используются для фильтрации записей на основе более чем одного условия:
Оператор AND отображает запись, если все условия, разделенные символом AND, имеют значение TRUE.
Оператор OR отображает запись, если любое из условий, разделенных OR, является TRUE.
Оператор NOT відображає запис, якщо умова (и) НЕ ПРАВИЛЬНА.
*/
-- AND
SELECT * FROM table_name
WHERE condition1 AND condition2 AND condition3;
-- OR
SELECT * FROM table_name
WHERE condition1 OR condition2 OR condition3;
-- NOT
SELECT * FROM table_name
WHERE NOT condition;
