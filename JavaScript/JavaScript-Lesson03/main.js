//Оператор typeof возвращает строку, указывающую тип операнда.
var n = 20;
var s = "Hello";
var b = true;
var age = null;
var o = { name: "Вася" };
var u;
document.write(typeof n +'<br>');
document.write(typeof s +'<br>');
document.write(typeof b +'<br>');
document.write(typeof age +'<br>');
document.write(typeof o +'<br>');
document.write(typeof u +'<br>');
