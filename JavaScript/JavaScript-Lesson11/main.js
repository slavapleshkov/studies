// Функция
function print(text) {
    document.write(text);
}
// Функция с использование локальной переменной
function local() {
    var message = 'Привет, я - Вася!';
    document.write(message)
}
// Функция с использование внешние переменной
var userName = 'Слава';
function global() {
    var message = 'Привет, я ' + userName;
    document.write(message);
}
print("hello");
local();
global();